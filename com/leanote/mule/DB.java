package com.leanote.mule;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class DB {
	private static DataSource dataSource;
	private static Connection con;
	private static HashMap<String, String> settings;
	private static long lastGetSettingsTime;
	
	private static final Logger LOGERROR = Logger.getLogger("leanote.error");
	
	public DB() {
	}
	
	public static Connection getConnection() {
	    if (dataSource == null) {
	        initDataSource();
	    }
	    try {
	        con = dataSource.getConnection();
	
	    } catch (SQLException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	    return con;
	}
	
	/** 关闭数据源 */  
    protected static void shutdownDataSource() throws SQLException {  
        BasicDataSource bds = (BasicDataSource) dataSource;  
        bds.close();
    }
	
	public static void initDataSource() {
	    BasicDataSource ds = new BasicDataSource();
	    
    	Properties prop = new Properties();  
//    	Common.println(DB.class.getClassLoader().getResource("inhope.properties"));
//    	 Common.println("---------life-----------");
//    	 file:/Applications/AnypointStudio.app/Contents/Eclipse/plugins/org.mule.tooling.server.3.8.0_6.0.0.201605131329/mule/conf/inhope.properties
//    	 Common.println(DB.class.getClassLoader().getResource("inhope.properties"));
    	LOGERROR.error("inhope.properties: " + DB.class.getClassLoader().getResource("inhope.properties"));
    	InputStream ins = DB.class.getClassLoader().getResourceAsStream("inhope.properties");
    	String jdbcUrl = "";
    	String jdbcUsername = "";
    	String jdbcPassword = "";
    	String jdbcDriver = "";
    	try {
            prop.load(ins);
            jdbcUrl = prop.getProperty("jdbc.url");
            jdbcUsername = prop.getProperty("jdbc.username");
            jdbcPassword = prop.getProperty("jdbc.password");
            jdbcDriver = prop.getProperty("jdbc.driver");
            
            Common.println("jdbcUrl: " + jdbcUrl);
            LOGERROR.error("jdbcUrl: " + jdbcUrl);
            
        } catch (IOException e) {
            e.printStackTrace();
            LOGERROR.error(e);
        }
	    ds.setUrl(jdbcUrl);
	    ds.setDriverClassName(jdbcDriver);
	    ds.setUsername(jdbcUsername);
	    ds.setPassword(jdbcPassword);
	
	    //3. 指定数据源的一些可选的属性.
        //1). 指定数据库连接池中初始化连接数的个数
	    ds.setInitialSize(50);
        
        //2). 指定最大的连接数: 同一时刻可以同时向数据库申请的连接数
        ds.setMaxActive(80);
        
        //3). 指定小连接数: 在数据库连接池中保存的最少的空闲连接的数量 
//        ds.setMinIdle(1);
        ds.setMaxIdle(50);
        
        //4).等待数据库连接池分配连接的最长时间. 单位为毫秒. 超出该时间将抛出异常. 
        ds.setMaxWait(1000 * 10);
	    ds.setRemoveAbandoned(true);
	    ds.setRemoveAbandonedTimeout(300);
	    dataSource = ds;
	}
	
	/* 用于测试连接状态的方法 */
	public static void print() {
	    BasicDataSource ds = (BasicDataSource) dataSource;
	    Common.println(ds.getInitialSize());
	    Common.println(ds.getNumActive());
	    Common.println(ds.getNumIdle());
	    Common.println(ds.getDefaultAutoCommit());
	}
	
	/*
     * 将rs结果转换成对象列表
     * @param rs jdbc结果集
     * @param clazz 对象的映射类
     * return 封装了对象的结果列表
     */
    public static List populate(ResultSet rs , Class clazz) {
    	List list = new ArrayList();
    	try {
	        //结果集的元素对象 
	        ResultSetMetaData rsmd = rs.getMetaData();
	        //获取结果集的元素个数
	         int colCount = rsmd.getColumnCount();
	//         Common.println("#");
	//         for(int i = 1;i<=colCount;i++){
	//             Common.println(rsmd.getColumnName(i));
	//             Common.println(rsmd.getColumnClassName(i));
	//             Common.println("#");
	//         }
	         //返回结果的列表集合
	         //业务对象的属性数组
	         Field[] fields = clazz.getDeclaredFields();
	         while(rs.next()){//对每一条记录进行操作
	             Object obj = clazz.newInstance();//构造业务对象实体
	             //将每一个字段取出进行赋值
	             for(int i = 1;i<=colCount;i++){
	                 Object value = rs.getObject(i);
	                 //寻找该列对应的对象属性
	                 for(int j=0;j<fields.length;j++){
	                     Field f = fields[j];
	                     //如果匹配进行赋值
	                     if(f.getName().equalsIgnoreCase(rsmd.getColumnName(i))){
	                         boolean flag = f.isAccessible();
	                         f.setAccessible(true);
	                         f.set(obj, value);
	                         f.setAccessible(flag);
	                     }
	                 }
	             }
	             list.add(obj);
	        }
	        return list;
    	} catch(Exception e) {
    		e.printStackTrace();
    		return list;
    	}
    }
    
    public static void closeConn(Connection conn) {
    	try {
    		conn.close();
    	} catch(Exception e) {
    	}
    }
	
	public static int insertLog(LogInfo log) {
		Common.println(" insert log....");
	    Connection conn = getConnection();
	    int i = 0;
	    String sql = "insert into log (" + 
	    		"apiName, " + 
	    		"url, " + 
	    		"queryStrings, " + 
	    		
	    		"accessDatetime, " + 
	    		"accessDate, " + 
	    		"accessTime, " + 
	    		"accessYear, " + 
	    		"accessMonth, " +
	    		"accessDay, " +
	    		
	    		"duration, " +
	    		"status, " +
	    		"exception, " +
	    		"method, " + 
	    		"isWS, " +
	    		"platformName " +
	    		")" + 
	    		" values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	    PreparedStatement pstmt;
	    try {
	    	int j = 1;
	        pstmt = (PreparedStatement) conn.prepareStatement(sql);
	        pstmt.setString(j++, log.getApiName());
	        pstmt.setString(j++, log.getUrl());
	        pstmt.setString(j++, log.getQueryStrings());
	        
	        pstmt.setTimestamp(j++, log.getAccessDatetime());
	        pstmt.setString(j++, log.getAccessDate());
	        pstmt.setString(j++, log.getAccessTime());
	        pstmt.setInt(j++, log.getAccessYear());
	        pstmt.setInt(j++, log.getAccessMonth()); // 这里有可能为null ?
	        pstmt.setInt(j++, log.getAccessDay());
	        
	        pstmt.setInt(j++, log.getDuration());
	        pstmt.setInt(j++, log.getStatus());
	        pstmt.setString(j++, log.getException());
	        pstmt.setString(j++, log.getMethod());
	        
	        pstmt.setBoolean(j++, log.getIsWS());
	        pstmt.setString(j++, log.getPlatformName());
	        
	        i = pstmt.executeUpdate();
	        pstmt.close();
	        conn.close();
	    } catch (SQLException e) {
	        e.printStackTrace();
	        closeConn(conn);
	    }
	    return i;
	}
	
	public static int insertApi(ApiInfo api) {
	    Connection conn = getConnection();
	    int i = 0;
	    
	    String values = "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?";
	    String lastAccessTimeCol = "lastAccessDatetime";
	    if (api.getLastFailureDatetime() != null) {
	    	lastAccessTimeCol += ", lastFailureDatetime";
	    	values += ",?";
	    }
	    
	    String sql = "insert into api (" + 
	    		"apiName, " + 
	    		"method, " + 
	    		"platformName, " + 
	    		"remark, " + 
	    		"status, " + 
	    		
	    		"averageDuration, " + 
	    		"durationThreshold, " + 
	    		"accessSuccessCount, " + 
	    		"accessFailureCount, " +
					    		
				"exception, " +
				"exceptionStatus, " +

				lastAccessTimeCol + 
	    		")" + 
	    		" values(" + values + ")";
	    PreparedStatement pstmt;
	    try {
	    	int j = 1;
	        pstmt = (PreparedStatement) conn.prepareStatement(sql);
	        pstmt.setString(j++, api.getApiName());
	        pstmt.setString(j++, api.getMethod());
	        
	        pstmt.setString(j++, api.getPlatformName());
	        pstmt.setString(j++, api.getRemark());
	        pstmt.setInt(j++, api.getStatus());
	        
	        pstmt.setDouble(j++, api.getAverageDuration());
	        pstmt.setInt(j++, api.getDurationThreshold());
	        pstmt.setInt(j++, api.getAccessSuccessCount());
	        pstmt.setInt(j++, api.getAccessFailureCount());
	        
	        pstmt.setString(j++, api.getException());
	        pstmt.setInt(j++, api.getExpcetionStatus());
	        
	        pstmt.setTimestamp(j++, api.getLastAccessDatetime());
	        
	        if (api.getLastFailureDatetime() != null) {
	        	pstmt.setTimestamp(j++, api.getLastFailureDatetime());
	        }
	        
	        i = pstmt.executeUpdate();
	        pstmt.close();
	        conn.close();
	    } catch (SQLException e) {
	        e.printStackTrace();
	        closeConn(conn);
	    }
	    return i;
	}
	
	public static int updateApi(ApiInfo api) {
	    Connection conn = getConnection();
	    int i = 0;
	    String lastAccessTimeCol = "lastAccessDatetime=?";
	    if (api.getStatus() != 1) {
	    	lastAccessTimeCol += ",lastFailureDatetime=?, exception=?, exceptionStatus=?";
	    }
	    String sql = "update api set status=?, averageDuration=?, accessSuccessCount=?, accessFailureCount=?, "
	    		+ lastAccessTimeCol + " where id=? ";
	    PreparedStatement pstmt;
	    try {
	    	int j = 1;
	        pstmt = (PreparedStatement) conn.prepareStatement(sql);
	        pstmt.setInt(j++, api.getStatus());
	        
	        pstmt.setDouble(j++, api.getAverageDuration());
	        pstmt.setInt(j++, api.getAccessSuccessCount());
	        int fc = api.getAccessFailureCount();
	        pstmt.setInt(j++, fc);
	        
	        pstmt.setTimestamp(j++, api.getLastAccessDatetime());
	        
	        if (api.getStatus() != 1) {
	        	pstmt.setTimestamp(j++, api.getLastFailureDatetime());
	        	pstmt.setString(j++, api.getException());
	        	pstmt.setInt(j++, api.getExpcetionStatus());
	        }
	        
	        pstmt.setLong(j++, api.getId());
	        i = pstmt.executeUpdate();
	        pstmt.close();
	        conn.close();
	    } catch (SQLException e) {
	        e.printStackTrace();
	        closeConn(conn);
	    }
	    return i;
	}
	
	/**
	 * 通过api+platformName查询
	 * @param api
	 * @param platformName
	 * @return
	 */
	public static ApiInfo getApi(String api, String method, String platformName) {
		Connection conn = getConnection();
	    String sql = "select * from api where apiName = ? and method = ? and platformName = ?";
	    PreparedStatement pstmt;
	    try {
	        pstmt = (PreparedStatement)conn.prepareStatement(sql);
	        pstmt.setString(1, api);
	        pstmt.setString(2, method);
	        pstmt.setString(3, platformName);
	        ResultSet rs = pstmt.executeQuery();
	        List list = populate(rs, ApiInfo.class);
	        if (!list.isEmpty()) {
	        	closeConn(conn);
	        	return (ApiInfo)list.get(0);
	        }
	        pstmt.close();
	        conn.close();
	    } catch (SQLException e) {
	        e.printStackTrace();
	        closeConn(conn);
	    }
		return null;
	}
	
	/**
	 * 获取所有settings
	 * @return
	 */
	public static HashMap<String, String> getSettings() {
		Connection conn = getConnection();
	    String sql = "select * from setting";
	    PreparedStatement pstmt;
	    HashMap<String, String> settings = new HashMap<String, String>();
	    try {
	        pstmt = (PreparedStatement)conn.prepareStatement(sql);
	        ResultSet rs = pstmt.executeQuery();
	        List list = populate(rs, SettingInfo.class);
	        if (!list.isEmpty()) {
	        	for(Object o : list) {
	        		SettingInfo so = (SettingInfo)o;
	        		settings.put(so.getKey(), so.getValue());
	        	}
	        	DB.settings = settings;
	        	DB.lastGetSettingsTime = new Date().getTime();
	        	closeConn(conn);
	        	return settings;
	        }
	        pstmt.close();
	        conn.close();
	    } catch (SQLException e) {
	        e.printStackTrace();
	        closeConn(conn);
	    }
		return settings;
	}
	
	public static int getAccessThreshold() {
		if (DB.settings == null) {
			DB.getSettings();
		}
		long now = new Date().getTime();
		if (now - DB.lastGetSettingsTime > 10 * 1000 * 60) { // 10分钟一次
			DB.getSettings();
		}
		
		String d = DB.settings.get("accessThreshold");
		if (d == null) {
			return 0;
		}
		return Integer.parseInt(d);
	}
}
