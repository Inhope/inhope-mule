package com.leanote.mule;

import java.net.URI;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.tomcat.jni.Time;
import org.mule.api.MuleMessage;

public class AsyncLogService {
	private static ConcurrentLinkedQueue<LogInfo> concurrentLinkedQueue = new ConcurrentLinkedQueue<LogInfo>();   
//	private static LinkedBlockingQueue<LogInfo> concurrentLinkedQueue = new LinkedBlockingQueue<LogInfo>();   

	public static boolean isStarted;

    public static void start() {  
    	if (isStarted) {
    		return;
    	}
    	
//    	Common.println("队列初始化");
    	
    	isStarted = true;
        
        // n个消费者
        int count = 2;
    	ExecutorService executorService = Executors.newFixedThreadPool(count); 
        for (int i = 0; i < count; i++) {
        	executorService.submit(new Consumer("consumer" + i)); 
        }
    }
    
    // 写到queue中
    public static void insertLog (MuleMessage msg, String resourceId, long endTime, URI sourceUrl) {
    	LogInfo log = LogService.parseMsg(msg, resourceId, endTime, sourceUrl);
    	Common.println("添加Log....");
    	
    	if (log != null) {
    		start();
//    		Common.println("加入到队列中....");
    		concurrentLinkedQueue.add(log);
    	} else {
    		Common.println("is null....");
    	}
    }

    // 异步消费log
    static class Consumer implements Runnable {  
        private String name;  
  
        public Consumer(String name) {  
            this.name = name;  
        }  
        public void run() {  
            while(true) {
//            	Common.println(name + " ???");
                try {
                	LogInfo log = concurrentLinkedQueue.poll();
                	if (log == null) {
//                		Common.println(name + " 休息");
                		Thread.sleep(1000);
                	} else {
                		Common.println(name + " 消费");
                		// 插入日志
                		LogService.insertLog(log);
                	}
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } 
            }  
        }  
    }  
}