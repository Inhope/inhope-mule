package com.leanote.mule;

import java.net.URL;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.mule.DefaultMuleEvent;
import org.mule.api.MuleContext;
import org.mule.api.MuleMessage;
import org.mule.api.context.notification.ExceptionStrategyNotificationListener;
import org.mule.context.notification.ExceptionStrategyNotification;
import org.mule.transformer.simple.ObjectToString;

public class LogListenerException implements ExceptionStrategyNotificationListener<ExceptionStrategyNotification> {
	
	@Override
	public void onNotification(ExceptionStrategyNotification notification) {
		Common.println("----------EXCEPTION START 4-------");
		DefaultMuleEvent e = (DefaultMuleEvent)notification.getSource();
		MuleMessage msg = e.getMessage();
		
//		LogService.insertLog(msg, notification.getResourceIdentifier(), notification.getTimestamp(), e.getMessageSourceURI());
		AsyncLogService.insertLog(msg, notification.getResourceIdentifier(), notification.getTimestamp(), e.getMessageSourceURI());
		
		Common.println(notification);
		Common.println(notification.getResourceIdentifier());
		Common.println(notification.getSource());
		Common.println("----------EXCEPTION END-------");
	}

}
