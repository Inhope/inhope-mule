package com.leanote.mule;

import java.sql.Timestamp;

public class ApiInfo {
	 private long id;
     private String apiName;
     private String method;
     private String platformName;
     private String remark;
     private int status;
     private Double averageDuration;
     private int durationThreshold = 0;
     private int accessSuccessCount = 0;
     private int accessFailureCount = 0;
     private Timestamp lastAccessDatetime;
     private Timestamp lastFailureDatetime;
     
     private String exception;
     private int expcetionStatus;
     
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getApiName() {
		return apiName;
	}
	public void setApiName(String name) {
		this.apiName = name;
	}
	public String getPlatformName() {
		return platformName;
	}
	public void setPlatformName(String groupName) {
		this.platformName = groupName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Double getAverageDuration() {
		return averageDuration;
	}
	public void setAverageDuration(Double avarageDuration) {
		this.averageDuration = avarageDuration;
	}
	public int getDurationThreshold() {
		return durationThreshold;
	}
	public void setDurationThreshold(int durationThreshold) {
		this.durationThreshold = durationThreshold;
	}
	public int getAccessSuccessCount() {
		return accessSuccessCount;
	}
	public void setAccessSuccessCount(int accessSuccessCount) {
		this.accessSuccessCount = accessSuccessCount;
	}
	public int getAccessFailureCount() {
		return accessFailureCount;
	}
	public void setAccessFailureCount(int accessFailureCount) {
		this.accessFailureCount = accessFailureCount;
	}
	public Timestamp getLastAccessDatetime() {
		return lastAccessDatetime;
	}
	public void setLastAccessDatetime(Timestamp lastAccessDatetime) {
		this.lastAccessDatetime = lastAccessDatetime;
	}
	public Timestamp getLastFailureDatetime() {
		return lastFailureDatetime;
	}
	public void setLastFailureDatetime(Timestamp lastFailureDatetime) {
		this.lastFailureDatetime = lastFailureDatetime;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	public int getExpcetionStatus() {
		return expcetionStatus;
	}
	public void setExpcetionStatus(int expcetionStatus) {
		this.expcetionStatus = expcetionStatus;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
     
     
}
