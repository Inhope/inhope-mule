package com.leanote.mule;

import java.sql.Timestamp;;

public class LogInfo {
	
	 private String id;
     private String apiName;
     private String url;
     private String queryStrings;
     private Timestamp accessDatetime;
     private String accessTime;
     private String accessDate;
     private int accessYear;
     private int accessMonth;
     private int accessDay;
     private int duration;
     private int status;
     private String exception;
     private String method;
     
     private Boolean isWS; // 是否是webservice
     private String platformName; // 平台 小i
    
	@Override
	public String toString() {
		return "apiName=" + apiName + ", url=" + url + ", queryStrings=" + queryStrings
				+ ", accessDatetime=" + accessDatetime + ", accessTime=" + accessTime + ", accessDate=" + accessDate
				+ ", accessYear=" + accessYear + ", accessMonth=" + accessMonth + ", accessDay=" + accessDay
				+ ", duration=" + duration + ", status=" + status + ", exception=" + exception + ", method=" + method
				+ ", isWS=" + isWS + ", platformName=" + platformName;
	}

	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public int getAccessDay() {
		return accessDay;
	}
	public void setAccessDay(int accessDay) {
		this.accessDay = accessDay;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getApiName() {
		return apiName;
	}
	public void setApiName(String api) {
		this.apiName = api;
	}
	public String getQueryStrings() {
		return queryStrings;
	}
	public void setQueryStrings(String queryStrings) {
		this.queryStrings = queryStrings;
	}
	public Timestamp getAccessDatetime() {
		return accessDatetime;
	}
	public void setAccessDatetime(Timestamp accessDatetime) {
		this.accessDatetime = accessDatetime;
	}
	public String getAccessTime() {
		return accessTime;
	}
	public void setAccessTime(String accessTime) {
		this.accessTime = accessTime;
	}
	public String getAccessDate() {
		return accessDate;
	}
	public void setAccessDate(String accessDate) {
		this.accessDate = accessDate;
	}
	public int getAccessYear() {
		return accessYear;
	}
	public void setAccessYear(int acessYear) {
		this.accessYear = acessYear;
	}
	public int getAccessMonth() {
		return accessMonth;
	}
	public void setAccessMonth(int accessMonth) {
		this.accessMonth = accessMonth;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Boolean getIsWS() {
		return isWS;
	}
	public void setIsWS(Boolean isWS) {
		this.isWS = isWS;
	}
	public String getPlatformName() {
		return platformName;
	}
	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}
     
}
