package com.leanote.mule;

import java.net.URI;
import java.util.Date;

import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;  
import org.mule.api.transformer.TransformerException;
import org.mule.api.transformer.TransformerMessagingException;
import org.mule.transformer.AbstractMessageTransformer;  
  
  
public class LogTransformer extends AbstractMessageTransformer { 
	
	private MuleEvent e;
	
	@Override
    public Object transform(Object src, MuleEvent event) throws TransformerMessagingException
    {
		this.e = event;
        return transform(src, getEncoding(src), event);
    }
  
    @Override  
    public Object transformMessage(MuleMessage msg, String outputEncoding)  
            throws TransformerException {
    	
//    	LogService.insertLog(msg, this.e.getFlowConstruct().getName(), new Date().getTime(), null);
    	AsyncLogService.insertLog(msg, this.e.getFlowConstruct().getName(), new Date().getTime(), null);
        
    	return msg.getPayload();
    }  
  
}  