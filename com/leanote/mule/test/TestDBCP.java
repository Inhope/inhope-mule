package com.leanote.mule.test;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import java.sql.PreparedStatement;

import java.sql.Statement;
import java.util.Properties;

public class TestDBCP {
	private static DataSource dataSource;
	private static Connection con;
	
	public TestDBCP() {
	}
	
	public static Connection getConnection() {
	    if (dataSource == null) {
	        initDataSource();
	    }
	    try {
	        con = dataSource.getConnection();
//	        print();
	
	    } catch (SQLException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	    return con;
	}
	
	/** 关闭数据源 */  
    protected static void shutdownDataSource() throws SQLException {  
        BasicDataSource bds = (BasicDataSource) dataSource;  
        bds.close();
    }  
	
	public static void initDataSource() {
		
	    BasicDataSource ds = new BasicDataSource();
	    ds.setUrl("jdbc:mysql://127.0.0.1:3306/cas?useUnicode=true&characterEncoding=utf-8");
	    ds.setDriverClassName("com.mysql.jdbc.Driver");
	    ds.setUsername("root");
	    ds.setPassword("root");
	
	    //3. 指定数据源的一些可选的属性.
        //1). 指定数据库连接池中初始化连接数的个数
	    ds.setInitialSize(5);
        
        //2). 指定最大的连接数: 同一时刻可以同时向数据库申请的连接数
        ds.setMaxActive(5);
        
        //3). 指定小连接数: 在数据库连接池中保存的最少的空闲连接的数量 
        ds.setMinIdle(2);
        
        //4).等待数据库连接池分配连接的最长时间. 单位为毫秒. 超出该时间将抛出异常. 
        ds.setMaxWait(1000 * 5);
	    ds.setRemoveAbandoned(true);
	    ds.setRemoveAbandonedTimeout(2000);
	    dataSource = ds;
	}
	
	/* 用于测试连接状态的方法 */
	public static void print() {
	    BasicDataSource ds = (BasicDataSource) dataSource;
	    System.out.println(ds.getInitialSize());
	    System.out.println(ds.getNumActive());
	    System.out.println(ds.getNumIdle());
	    System.out.println(ds.getDefaultAutoCommit());
	}
	
	public void t() {
    	Properties prop = new Properties();  
    	InputStream ins = this.getClass().getResourceAsStream("/mule-delopy.properties");
    	try {
            prop.load(ins);
            String main = prop.getProperty("domain");
            System.out.println("--------");
            System.out.println(main);
        } catch (IOException e) {
            e.printStackTrace();    
        }
    }
	
	public static void main(String[] args) {
		TestDBCP t = new TestDBCP();
		t.t();
		
	    Connection con;
	    try {
	        con = TestDBCP.getConnection();
    	    String sql = "insert into users (username, password) values(?,?)";
    	    PreparedStatement pstmt;
    	    try {
    	        pstmt = con.prepareStatement(sql);
    	        pstmt.setString(1, "你好");
    	        pstmt.setString(2, "life好");
    	        pstmt.executeUpdate();
    	        pstmt.close();
    	    } catch (SQLException e) {
    	        e.printStackTrace();
    	    }
	    	
//	        print();
	        Statement stmt = con.createStatement();
	        ResultSet result = stmt.executeQuery("select * from log");
	        while (result.next()) {
	            String api = result.getString("api");
	            System.out.println(api);
	        }
	    } catch (SQLException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	}   
}