package com.leanote.mule.test;

import com.leanote.mule.Mail;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

public class Test {
	private static Connection conn;
	private static Connection getConn() {
	    String driver = "com.mysql.jdbc.Driver";
	    String url = "jdbc:mysql://127.0.0.1:3306/cas?useUnicode=true&characterEncoding=utf-8";
	    String username = "root";
	    String password = "root";
	    if (conn != null) {
	    	return conn;
	    }
	    try {
	        Class.forName(driver); //classLoader,加载对应驱动
	        conn = (Connection) DriverManager.getConnection(url, username, password);
	    } catch (ClassNotFoundException e) {
	        e.printStackTrace();
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	    return conn;
	}
	
	public static int insert(String inf, String remark) {
	    Connection conn = getConn();
	    int i = 0;
	    String sql = "insert into log (interface, remark) values(?,?)";
	    PreparedStatement pstmt;
	    try {
	        pstmt = (PreparedStatement) conn.prepareStatement(sql);
	        pstmt.setString(1, inf);
	        pstmt.setString(2, remark);
	        i = pstmt.executeUpdate();
	        pstmt.close();
	        conn.close();
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	    return i;
	}
	
	public static void main(String[] args) {
//		Connection conn = getConn();
//		insert("life", "你好");
		System.out.println("life");
		
		String smtp = "smtp.ym.163.com";  
	    String from = "noreply@leanote.com";  
	    String to = "life@leanote.com";  
//	    String copyto = "抄送人";
	    String subject = "邮件主题";  
	    String content = "邮件内容";  
	    String username="noreply@leanote.com";  
	    String password="abc123";  
	   
	    Mail.send(smtp, from, to, subject, content, username, password);  
		

		Calendar calendar = Calendar.getInstance();
        // 或者用 Date 来初始化 Calendar 对象  
        calendar.setTime(new java.util.Date());
        
     // 显示年份  
        int year = calendar.get(Calendar.YEAR);  
        System.out.println("year is = " + String.valueOf(year));  
      
        // 显示月份 (从0开始, 实际显示要加一)  
        int month = calendar.get(Calendar.MONTH);  
        System.out.println("nth is = " + (month + 1));  
      
        // 本周几  
        int week = calendar.get(Calendar.DAY_OF_WEEK);  
        System.out.println("week is = " + week);  
      
        // 今年的第 N 天  
        int DAY_OF_YEAR = calendar.get(Calendar.DAY_OF_YEAR);  
        System.out.println("DAY_OF_YEAR is = " + DAY_OF_YEAR);  
      
        // 本月第 N 天  
        int DAY_OF_MONTH = calendar.get(Calendar.DAY_OF_MONTH);  
        System.out.println("DAY_OF_MONTH = " + String.valueOf(DAY_OF_MONTH));
	}
}
