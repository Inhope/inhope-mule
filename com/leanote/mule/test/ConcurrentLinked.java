package com.leanote.mule.test;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.python.modules.time.Time;

public class ConcurrentLinked {  

	private static ConcurrentLinkedQueue<Integer> concurrentLinkedQueue = new ConcurrentLinkedQueue<Integer>();   
 
    public static void main(String[] args) {  
        ExecutorService executorService = Executors.newFixedThreadPool(2); 

//        executorService.submit(new Producer("producer1"));  
//        executorService.submit(new Producer("producer2"));  
//        executorService.submit(new Producer("producer3"));  
        executorService.submit(new Consumer("consumer1"));  
        executorService.submit(new Consumer("consumer2"));  
        executorService.submit(new Consumer("consumer3"));
        
        
        concurrentLinkedQueue.add(10); 
        concurrentLinkedQueue.add(12); 
    }  
  
    static class Producer implements Runnable {  
        private String name;
  
        public Producer(String name) {  
            this.name = name;  
        }  
  
        public void run() {  
            for (int i = 1; i < 10; ++i) {  
                System.out.println(name + " 生产 " + i);  
                concurrentLinkedQueue.add(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                //System.out.println(name+"end producer " + i);  
            }  
        }  
    }  
  
    static class Consumer implements Runnable {  
        private String name;  
  
        public Consumer(String name) {  
            this.name = name;  
        }  
        public void run() {  
            while(true) {
                try {
 
                	Integer a = concurrentLinkedQueue.poll();
                	if (a == null) {
                		System.out.println("sleep " + name);
                		Time.sleep(1);
                	} else {
                		System.out.println(name+" Consumer " +  a);
                	}

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }  
//                System.out.println();  
//                System.out.println(name+" end Consumer " + i);  
            }  
        }  
    }  
}