package com.leanote.mule;

import java.net.URI;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.mule.api.ExceptionPayload;
import org.mule.api.MuleMessage;
import org.mule.transport.http.HttpResponse;

public class LogService {
	
	private static final Logger LOG = Logger.getLogger("leanote.access");
	private static final Logger LOGERROR = Logger.getLogger("leanote.error");
	
	// 同步插入log
	public static void insertLog (MuleMessage msg, String resourceId, long endTime, URI sourceUrl) {
		LogInfo log = parseMsg(msg, resourceId, endTime, sourceUrl);
		if (log != null) {
			insertLog(log);
		}
    }
	
	/**
	 * 处理msg得到LogInfo
	 * 
	 * @param msg
	 * @param resourceId
	 * @param endTime
	 * @param sourceUrl 当是http proxy时exception msg里没有其它信息
	 */
	public static LogInfo parseMsg(MuleMessage msg, String resourceId, long endTime, URI sourceUrl) {
		// 要插入日志中
		int status = 404;
		long duration = 0;

		LogInfo log = new LogInfo();
		
		try {
			ExceptionPayload exception = msg.getExceptionPayload();
			if (exception != null) {
				String expceptionMsg = exception.getMessage();
				status = exception.getCode();
				Object obj = msg.getPayload();
				String extraExceptionMsg = "";
				if (obj != null) {
					if (obj instanceof HttpResponse) {
						HttpResponse res = (HttpResponse) obj;
						status = res.getStatuscode();
						extraExceptionMsg = res.getBodyAsString();
					} else if (obj instanceof String) {
						extraExceptionMsg = obj.toString();
					}
					if (extraExceptionMsg != "" && !extraExceptionMsg.equals(expceptionMsg)) {
						expceptionMsg = extraExceptionMsg + "\n----------\n" + expceptionMsg;
					}
				}
				if (expceptionMsg.contains("404")) {
					status = 404;
				} else if (expceptionMsg.contains("500")) {
					status = 500;
				}
				log.setException(expceptionMsg);
			} else {
				Object statusObj = msg.getOutboundProperty("http.status");
				if (statusObj == null) {
					return null;
				}
				if (statusObj instanceof String) {
					status = Integer.parseInt(statusObj.toString());
				} else if (statusObj instanceof Integer) {
					status = (int)statusObj;
				}
			}
			
			Object startObj = msg.getSessionProperty("startTime");
			if (startObj != null) {
				Long start = (Long)startObj;
				duration = endTime - start;
			}
			
			// 从outbound中找
			String url = (String)msg.getOutboundProperty("http.request"); // http://localhost:8000/HelloWorld
			String queryStrings = (String)msg.getOutboundProperty("http.query.string");
			String method = (String)msg.getOutboundProperty("http.method");
			
			// 从inbound中找
			if (url == null || method == null) {
				url = (String)msg.getInboundProperty("http.context.uri");
				queryStrings = (String)msg.getInboundProperty("http.query.string");
				method = (String)msg.getInboundProperty("http.method");
			}
			
			// 从session中找
			if (url == null || method == null) {
				url = (String)msg.getSessionProperty("http.context.uri");
				queryStrings = (String)msg.getSessionProperty("http.query.string");
				method = (String)msg.getSessionProperty("http.method");
				
				if (url != null && !url.isEmpty()) {
					if (url.indexOf("http") < 0) {
						url = "http://" + url;
					}
				}
			}
			
			if (url == null && sourceUrl != null) {
				url = sourceUrl.toString();
			}
			if (method == null) {
				method = "UNKOWN";
			}
			
			String api = "";
			String path = "";
			if (url != null) {
				URL urlObj = new URL(url);
				path = urlObj.getPath();
			}
			
			Object SOAPAction = msg.getSessionProperty("SOAPAction");
			log.setIsWS(false);
			if (SOAPAction != null) {
				log.setIsWS(true);
				
				String SOAPActionStr = (String)SOAPAction; // "http://example/HelloWorld/sayHelloWorldFromRequest"
				SOAPActionStr = SOAPActionStr.replace("\"", "");
				SOAPActionStr = SOAPActionStr.replace("\"", "");
				int index = SOAPActionStr.indexOf(path);
				if (index >= 0) {
					api = SOAPActionStr.substring(index);
				} else {
					api = SOAPActionStr;
				}
			} else {
				api = path;
			}
			
//			String resourceId = notification.getResourceIdentifier(); // xiaoi-api1, xiaoi-api2
			String platformName = resourceId;
			int index2 = platformName.indexOf("-");
			if (index2 > 0) {
				platformName = platformName.substring(0, index2);
			}
			log.setPlatformName(platformName);
			
			log.setApiName(api);
			log.setUrl(url);
			log.setDuration((int)duration);
			log.setMethod(method);
			log.setStatus(status);
			log.setQueryStrings(queryStrings);
			
			java.util.Date date1 = new java.util.Date();
			Calendar calendar = Calendar.getInstance();
	        // 或者用 Date 来初始化 Calendar 对象  
	        calendar.setTime(new java.util.Date());
			
			log.setAccessDatetime(new Timestamp(date1.getTime()));
			log.setAccessDay(calendar.get(Calendar.DAY_OF_MONTH));
			log.setAccessMonth(calendar.get(Calendar.MONTH)+1); // 这里有可能为null? 不可能啊
			log.setAccessYear(calendar.get(Calendar.YEAR));
			
			SimpleDateFormat fmtDate = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat fmtMinute = new SimpleDateFormat("HH:mm"); // :ss
			log.setAccessDate(fmtDate.format(date1));
			log.setAccessTime(fmtMinute.format(date1));
			
			return log;
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * 插入Log
	 * 异步来消费更好
	 * @param log
	 */
	public static void insertLog(LogInfo log) {
		// 插入
		// TODO, 如果性能有影响, 这里可以注释掉, 不要插入每一条日志了, 也没用
		try {
//			DB.insertLog(log);
			LOG.error(log);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		// 更新api
		ApiInfo api = DB.getApi(log.getApiName(), log.getMethod(), log.getPlatformName());
		Timestamp time = new Timestamp(new Date().getTime());
		Boolean ok = log.getStatus() == 200 || log.getStatus() == 304;
		// 插入
		if (api == null) {
			api = new ApiInfo();
			api.setPlatformName(log.getPlatformName());
			api.setMethod(log.getMethod());
			api.setApiName(log.getApiName());
			
			api.setStatus(ok ? 1 : 0);
			api.setDurationThreshold(0);
			api.setAccessSuccessCount(ok ? 1 : 0);
			api.setAccessFailureCount(!ok ? 1 : 0);
			api.setAverageDuration((double)log.getDuration());
			
			api.setLastAccessDatetime(time);
			if (!ok) {
				api.setLastFailureDatetime(time);
				api.setException(api.getException());
				api.setExpcetionStatus(api.getStatus());
			} else {
				api.setException("");
				api.setExpcetionStatus(0);
			}
			
			DB.insertApi(api);
		// 更新
		} else {
			api.setStatus(ok ? 1 : 0);
			api.setLastAccessDatetime(time);
			if (ok) {
				api.setAverageDuration( ( (api.getAccessSuccessCount() * api.getAverageDuration()) + (double)log.getDuration() ) /  (api.getAccessSuccessCount() + 1) );
				api.setAccessSuccessCount(api.getAccessSuccessCount() + 1);
				
			} else {
				api.setAccessFailureCount(api.getAccessFailureCount() + 1);
				api.setLastFailureDatetime(time);
				
				api.setException(log.getException());
				api.setExpcetionStatus(log.getStatus());
			}
			
			// 这里可以批量更新
			DB.updateApi(api);
		}
		
		// 如果是exception, 发邮件
		if (log.getException() != null && !log.getException().isEmpty()) {
			String subject = "[Api异常] " + log.getStatus() + " " + log.getApiName();  
		    String content = log.getException();
		    LOGERROR.error(subject + " " + content);
			sendEmail(subject, content);
			
		} else {
			// 是否超时
			int threshold = DB.getAccessThreshold();
			Common.println(threshold + " getAccessThreshold");
			if (threshold > 0 && threshold < log.getDuration()) {
				String subject = "[Api超时] " + log.getDuration() + " " + log.getApiName();  
			    String content = "时长: " + log.getDuration() + "; 阈值: " + threshold;
			    
			    LOGERROR.error(subject + " " + content);
				sendEmail(subject, content);
			}
		}
	}

	// 发送异常邮件
	private static void sendEmail(String subject, String content) {
		Common.println("发送邮件");
		
		HashMap<String, String> settings = DB.getSettings();
		
	    String from = settings.get("emailUser"); 
	    String port = settings.get("emailPort");
	    String to = settings.get("muleEmailTo");
	    String cc = settings.get("muleEmailCC");
	    
	    Boolean ssl = false;
	    String useSSL = settings.get("emailSSL");
	    if (useSSL != null && useSSL.equals("true")) {
	    	ssl = true;
	    }
	    
	    Mail.sendAndCc(settings.get("emailHost"), port, 
	    		from, settings.get("emailPassword"),
	    		ssl,
	    		from, to, cc, 
	    		subject, content);
	}

}
